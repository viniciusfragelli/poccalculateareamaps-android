package br.com.livetouch.pocarea;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.maps.android.SphericalUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MapsJavaActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener {

    private GoogleMap map;
    private Polygon polygon;
    private boolean isAddPolygon = false;
    private List<LatLng> listaPontos;
    private Marker tempMark;
    private TextView tvArea;
    private LinearLayout llExcluir;
    private FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_java);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        tvArea = findViewById(R.id.tvArea);
        llExcluir = findViewById(R.id.llExcluir);
        frameLayout = findViewById(R.id.frameLayout);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        listaPontos = new ArrayList<>();
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-25.445997587450147, -49.287230148911476), 19));
        googleMap.setOnMapClickListener(this);
        googleMap.setOnMarkerClickListener(this);
        map = googleMap;
    }

    private void stylePolygon(Polygon polygon) {
        List<PatternItem> pattern = Arrays.asList(new Gap(20), new Dash(20));
        polygon.setStrokePattern(pattern);
        polygon.setStrokeWidth(8);
        polygon.setStrokeColor(0xff388E3C);
        polygon.setFillColor(0xff81C784);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        if(tempMark == null){
            listaPontos.add(latLng);
            drawPolygon();
            drawMarks(latLng);
        }else{
            replaceNewPositionMarkers(latLng);
            drawPolygon();
            drawMarks(latLng);
        }
    }

    private void replaceNewPositionMarkers(LatLng latLng){
        for(LatLng l : listaPontos){
            if(l.longitude == tempMark.getPosition().longitude && l.latitude == tempMark.getPosition().latitude){
                listaPontos.remove(l);
                listaPontos.add(latLng);
                tempMark.remove();
                break;
            }
        }
        tempMark = null;
        TransitionManager.beginDelayedTransition(frameLayout);
        llExcluir.setVisibility(View.GONE);
    }

    private void drawPolygon(){
        if(listaPontos.size() >= 3){
            if(isAddPolygon && polygon != null)polygon.remove();
            listaPontos = OrdernarVetices.ordenarPorVertices(listaPontos);
            polygon = map.addPolygon(createPolygonOptions());
            polygon.setTag("alpha");
            stylePolygon(polygon);
            isAddPolygon = true;
            double area = SphericalUtil.computeArea(polygon.getPoints());
            tvArea.setText("Área: "+area+" m²");
        }
    }
    private void drawMarks(LatLng latLng){
        map.addMarker(new MarkerOptions().position(latLng).title("Ponto "+listaPontos.size()));
    }

    private PolygonOptions createPolygonOptions(){
        PolygonOptions polygonOptions = new PolygonOptions();
        for(LatLng p : listaPontos){
            polygonOptions.add(p);
        }
        return polygonOptions;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        this.tempMark = marker;
        TransitionManager.beginDelayedTransition(frameLayout);
        llExcluir.setVisibility(View.VISIBLE);
        return false;
    }

    public void onClickExcluirMarker(View view) {
        if(tempMark != null){
            for(LatLng l : listaPontos) {
                if (l.longitude == tempMark.getPosition().longitude && l.latitude == tempMark.getPosition().latitude) {
                    listaPontos.remove(l);
                    tempMark.remove();
                    tempMark = null;
                    break;
                }
            }
        }
        TransitionManager.beginDelayedTransition(frameLayout);
        llExcluir.setVisibility(View.GONE);
        if(listaPontos.size() < 3){
            polygon.remove();
            polygon = null;
        }
        drawPolygon();
    }
}