package br.com.livetouch.pocarea;

import com.google.android.gms.maps.model.LatLng;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by vinic on 08/10/2018.
 */

public class OrdernarVetices {
    public static LatLng findCentroid(List<LatLng> points) {
        double x = 0;
        double y = 0;
        for (LatLng p : points) {
            x += p.latitude;
            y += p.longitude;
        }
        double newlat;
        double newlog;
        newlat = x / ((double) points.size());
        newlog = y / ((double) points.size());
        LatLng center = new LatLng(newlat, newlog);
        return center;
    }

    public static List<LatLng> ordenarPorVertices(List<LatLng> points) {
        final LatLng center = findCentroid(points);
        Collections.sort(points, new Comparator<LatLng>() {
            @Override
            public int compare(LatLng a, LatLng b) {
                    double a1 = (Math.toDegrees(Math.atan2(a.latitude - center.latitude, a.longitude - center.longitude)) + 360) % 360;
                    double a2 = (Math.toDegrees(Math.atan2(b.latitude - center.latitude, b.longitude - center.longitude)) + 360) % 360;
                    return (int) (a1 - a2);
            }
        });
        return points;
    }
}
